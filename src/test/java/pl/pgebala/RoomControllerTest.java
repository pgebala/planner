package pl.pgebala;

import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.pgebala.model.Room;
import pl.pgebala.service.RoomService;
import pl.pgebala.service.impl.RoomServiceImpl;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

@QuarkusTest
public class RoomControllerTest {

    @BeforeAll
    public static void setup() {
        RoomService mock = Mockito.mock(RoomServiceImpl.class);
        Mockito.when(mock.findAll()).thenReturn(getRooms());
        Mockito.when(mock.save(anyLong(), any(Room.class))).thenReturn(getRoom(3L, "R3"));
        Mockito.when(mock.update(any(Room.class))).thenReturn(getRoom(1L, "RP1"));
        Mockito.when(mock.delete(1L)).thenReturn(true);
        QuarkusMock.installMockForType(mock, RoomService.class);
    }

    @Test
    public void testRoomGetEndpoint() {
        given()
            .when()
                .get("/api/room")
            .then()
                .statusCode(200)
                .body("$.size()", is(2),
                        "[0].id", is(1),
                        "[0].name", is("R1"),
                        "[1].id", is(2),
                        "[1].name", is("R2")
                );
    }

    @Test
    public void testRoomPostEndpoint() {
        given()
            .when()
                .contentType(ContentType.JSON)
                .body(getRoom(3L, "R3"))
                .post("/api/room/1")
            .then()
                .statusCode(200)
                .body("id", is(3),
                        "name", is("R3")
                );
    }

    @Test
    public void testRoomPutEndpoint() {
        given()
            .when()
                .contentType(ContentType.JSON)
                .body(getRoom(1L, "RP1"))
                .put("/api/room")
            .then()
                .statusCode(200)
                .body("id", is(1),
                        "name", is("RP1")
                );
    }

    @Test
    public void testRoomDeleteEndpointTrue() {
        given()
                .when()
                .delete("/api/room/1")
                .then()
                .statusCode(204);
    }

    @Test
    public void testRoomDeleteEndpointFalse() {
        given()
                .when()
                .delete("/api/room/3")
                .then()
                .statusCode(404);
    }

    private static List<Room> getRooms() {
        return Arrays.asList(getRoom(1L, "R1"), getRoom(2L, "R2"));
    }

    private static Room getRoom(Long id, String name) {
        Room a1 = new Room();
        a1.id = id;
        a1.name = name;
        return a1;
    }
}
