package pl.pgebala;

import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.pgebala.model.Item;
import pl.pgebala.model.dto.ItemDto;
import pl.pgebala.service.ItemService;
import pl.pgebala.service.impl.ItemServiceImpl;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

@QuarkusTest
public class ItemControllerTest {

    @BeforeAll
    public static void setup() {
        ItemService mock = Mockito.mock(ItemServiceImpl.class);
        Mockito.when(mock.findAll()).thenReturn(getItemDtos());
        Mockito.when(mock.save(anyLong(), any(Item.class))).thenReturn(getItemDto(3L, "R3"));
        Mockito.when(mock.update(any(Item.class))).thenReturn(getItemDto(1L, "RP1"));
        Mockito.when(mock.findByName(any(String.class))).thenReturn(getItemDto(1L, "R1"));
        Mockito.when(mock.delete(1L)).thenReturn(true);
        QuarkusMock.installMockForType(mock, ItemService.class);
    }

    @Test
    public void testItemGetEndpoint() {
        given()
                .when()
                .get("/api/item")
                .then()
                .statusCode(200)
                .body("$.size()", is(2),
                        "[0].id", is(1),
                        "[0].name", is("R1"),
                        "[1].id", is(2),
                        "[1].name", is("R2")
                );
    }

    @Test
    public void testItemGetByNameEndpoint() {
        given()
                .when()
                .get("/api/item/name/R1")
                .then()
                .statusCode(200)
                .body("id", is(1),
                        "name", is("R1")
                );
    }

    @Test
    public void testItemPostEndpoint() {
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(getItemDto(3L, "R3"))
                .post("/api/item/1")
                .then()
                .statusCode(200)
                .body("id", is(3),
                        "name", is("R3")
                );
    }

    @Test
    public void testItemPutEndpoint() {
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(getItemDto(1L, "RP1"))
                .put("/api/item")
                .then()
                .statusCode(200)
                .body("id", is(1),
                        "name", is("RP1")
                );
    }

    @Test
    public void testItemDeleteEndpointTrue() {
        given()
                .when()
                .delete("/api/item/1")
                .then()
                .statusCode(204);
    }

    @Test
    public void testItemDeleteEndpointFalse() {
        given()
                .when()
                .delete("/api/item/3")
                .then()
                .statusCode(404);
    }

    private static List<ItemDto> getItemDtos() {
        return Arrays.asList(getItemDto(1L, "R1"), getItemDto(2L, "R2"));
    }

    private static ItemDto getItemDto(Long id, String name) {
        return ItemDto.builder().id(id).name(name).build();
    }

}
