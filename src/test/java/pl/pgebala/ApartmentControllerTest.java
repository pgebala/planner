package pl.pgebala;

import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.pgebala.model.Apartment;
import pl.pgebala.service.ApartmentService;
import pl.pgebala.service.impl.ApartmentServiceImpl;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;

@QuarkusTest
public class ApartmentControllerTest {

    @BeforeAll
    public static void setup() {
        ApartmentService mock = Mockito.mock(ApartmentServiceImpl.class);
        Mockito.when(mock.findAll()).thenReturn(getApartments());
        Mockito.when(mock.save(any(Apartment.class))).thenReturn(getApartment(3L, "A3"));
        Mockito.when(mock.update(any(Apartment.class))).thenReturn(getApartment(1L, "AP1"));
        Mockito.when(mock.delete(1L)).thenReturn(true);
        QuarkusMock.installMockForType(mock, ApartmentService.class);
    }

    @Test
    public void testApartmentGetEndpoint() {
        given()
            .when()
                .get("/api/apartment")
            .then()
                .statusCode(200)
                .body("$.size()", is(2),
                        "[0].id", is(1),
                        "[0].name", is("A1"),
                        "[1].id", is(2),
                        "[1].name", is("A2")
                );
    }

    @Test
    public void testApartmentPostEndpoint() {
        given()
            .when()
                .contentType(ContentType.JSON)
                .body(getApartment(3L, "A3"))
                .post("/api/apartment")
            .then()
                .statusCode(200)
                .body("id", is(3),
                        "name", is("A3")
                );
    }

    @Test
    public void testApartmentPutEndpoint() {
        given()
            .when()
                .contentType(ContentType.JSON)
                .body(getApartment(1L, "AP1"))
                .put("/api/apartment")
            .then()
                .statusCode(200)
                .body("id", is(1),
                        "name", is("AP1")
                );
    }

    @Test
    public void testApartmentDeleteEndpointTrue() {
        given()
                .when()
                .delete("/api/apartment/1")
                .then()
                .statusCode(204);
    }

    @Test
    public void testApartmentDeleteEndpointFalse() {
        given()
                .when()
                .delete("/api/apartment/3")
                .then()
                .statusCode(404);
    }

    private static List<Apartment> getApartments() {
        return Arrays.asList(getApartment(1L, "A1"), getApartment(2L, "A2"));
    }

    private static Apartment getApartment(Long id, String name) {
        Apartment a1 = new Apartment();
        a1.id = id;
        a1.name = name;
        return a1;
    }
}
