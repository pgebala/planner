INSERT INTO apartment(id, name) VALUES (999999, 'Piasta 2A');
INSERT INTO apartment(id, name) VALUES (999998, 'Szkieletor 5B');

INSERT INTO room(id, name, apartment_id) VALUES (899999, 'Kuchnia', 999999);
INSERT INTO room(id, name, apartment_id) VALUES (899998, 'Salon', 999999);
INSERT INTO room(id, name, apartment_id) VALUES (899997, 'Salon', 999998);

INSERT INTO item(id, name, type, unit, quantity, price, room_id) VALUES (799999, 'Lodówka', 'EQUIPMENT', 'PIECE', 1, 2000.00, 899999);
INSERT INTO item(id, name, type, unit, quantity, price, room_id) VALUES (799998, 'Rolety', 'EQUIPMENT', 'PIECE', 2, 120.00, 899999);
INSERT INTO item(id, name, type, unit, quantity, price, room_id) VALUES (799997, 'Montaż rolet', 'SERVICE', 'HOUR', 1, 40.00, 899999);
INSERT INTO item(id, name, type, unit, quantity, price, room_id) VALUES (799996, 'Farba', 'SUPPLY', 'SQUARE_METER', 10, 40.00, 899998);
INSERT INTO item(id, name, type, unit, quantity, price, room_id) VALUES (799995, 'Malowanie', 'SERVICE', 'SQUARE_METER', 10, 50.00, 899998);
