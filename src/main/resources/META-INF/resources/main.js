var app = angular.module("ApartmentManagement", []);

//Controller Part
app.controller("ApartmentController", function ($scope, $http) {

    //Initialize page with empty data
    $scope.apartments = [];
    $scope.selectedApartment = null;
    $scope.rooms = [];
    $scope.selectedRoom = null;
    $scope.items = [];

    $scope.showNewApartmentForm = false;
    $scope.showNewRoomForm = false;
    $scope.showNewItemForm = false;

    $scope.apartmentForm = {
        name: ""
    };

    $scope.roomForm = {
        name: ""
    };

    $scope.itemForm = {
        name: "",
        type: "",
        unit: "",
        quantity: null,
        price: null
    };

    //Now load the data from server
    _refreshApartmentsData();
    _refreshRoomsData();
    _refreshItemData();

    // APARTMENT

    //HTTP POST methods for add data
    $scope.addApartment = function () {
        $http({
            method: "POST",
            url: '/api/apartment',
            data: angular.toJson($scope.apartmentForm),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(_successApartment, _error);
    };

    //HTTP GET- get all apartments collection
    function _refreshApartmentsData() {
        $http({
            method: 'GET',
            url: '/api/apartment'
        }).then(function successCallback(response) {
            $scope.apartments = response.data;
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
    }

    $scope.newApartment = function() {
        $scope.selectedApartment = null;
        $scope.selectedRoom = null;
        $scope.showNewApartmentForm = true;
        $scope.showNewRoomForm = false;
    }

    $scope.selectApartment = function () {
        $scope.showNewItemForm = false;
        _refreshRoomsData();
        _refreshItemData();
    }

    function _successApartment(response) {
        _refreshApartmentsData();
        $scope.apartmentForm.name = "";
        $scope.showNewApartmentForm = false;
        $scope.showNewRoomForm = false;
    }

    // ROOM

    $scope.addRoom = function () {
        $http({
            method: "POST",
            url: '/api/room/' + $scope.selectedApartment,
            data: angular.toJson($scope.roomForm),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(_successRoom, _error);
    };

    //HTTP GET- get all rooms by apartment collection
    function _refreshRoomsData() {
        $http({
            method: 'GET',
            url: '/api/room/' + $scope.selectedApartment
        }).then(function successCallback(response) {
            $scope.rooms = response.data;
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
    }

    $scope.selectRoom = function () {
        $scope.showNewItemForm = true;
        _refreshItemData();
    }

    $scope.newRoom = function() {
        $scope.selectedRoom = null;
        $scope.showNewApartmentForm = false;
        $scope.showNewRoomForm = true;
    }

    function _successRoom(response) {
        _refreshRoomsData();
        $scope.roomForm.name = "";
        $scope.showNewApartmentForm = false;
        $scope.showNewRoomForm = false;
    }

    // ITEM
    //HTTP GET- get all items by room collection
    function _refreshItemData() {
        $http({
            method: 'GET',
            url: '/api/item/' + $scope.selectedRoom
        }).then(function successCallback(response) {
            $scope.items = response.data;
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
    }

    $scope.addItem = function () {

        $http({
            method: "POST",
            url: '/api/item/' + $scope.selectedRoom,
            data: angular.toJson($scope.itemForm),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(_successItem, _error);
    };

    function _successItem(response) {
        _refreshItemData();
        $scope.itemForm.name = "";
        $scope.itemForm.type = "";
        $scope.itemForm.unit = "";
        $scope.itemForm.quantity = null;
        $scope.itemForm.price = null;
    }

    $scope.deleteItem = function (id) {
        $http({
            method: 'DELETE',
            url: '/api/item/' + id
        }).then(function successCallback(response) {
            _refreshItemData()
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
    }

    function _error(response) {
        alert(response.data.message || response.statusText);
    }
});
