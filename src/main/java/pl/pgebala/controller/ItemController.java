package pl.pgebala.controller;

import pl.pgebala.model.Item;
import pl.pgebala.model.dto.ItemDto;
import pl.pgebala.service.ItemService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/item")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ItemController {

    @Inject
    ItemService itemService;

    @GET
    public Response getAllItems() {
        return Response.ok(itemService.findAll()).build();
    }

    @GET
    @Path("{roomId}")
    public Response getAllRooms(@PathParam("roomId") Long roomId) {
        return Response.ok(itemService.findAllByRoomId(roomId)).build();
    }

    @GET
    @Path("name/{name}")
    public Response getByName(@PathParam("name") String name) {
        return Response.ok(itemService.findByName(name)).build();
    }

    @POST
    @Path("{roomId}")
    public Response saveItem(@PathParam("roomId") Long roomId, Item item) {
        return Response.ok(itemService.save(roomId, item)).build();
    }

    @PUT
    public Response updateItem(Item item) {
        ItemDto updatedItem = itemService.update(item);
        return updatedItem != null ? Response.ok(updatedItem).build()
                : Response.status(Response.Status.NOT_FOUND).entity("Entity not found for ID: " + item.id).build();
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") Long id) {
        return itemService.delete(id) ? Response.noContent().build()
                : Response.status(Response.Status.NOT_FOUND).entity("Entity not found for ID: " + id).build();
    }
}
