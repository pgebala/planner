package pl.pgebala.controller;

import pl.pgebala.model.Apartment;
import pl.pgebala.service.ApartmentService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/apartment")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ApartmentController {

    @Inject
    ApartmentService apartmentService;

    @GET
    public Response getAllApartments() {
        return Response.ok(apartmentService.findAll()).build();
    }

    @POST
    public Response saveApartment(Apartment apartment) {
        return Response.ok(apartmentService.save(apartment)).build();
    }

    @PUT
    public Response updateApartment(Apartment apartment) {
        Apartment updatedApartment = apartmentService.update(apartment);
        return updatedApartment != null ? Response.ok(updatedApartment).build()
                : Response.status(Response.Status.NOT_FOUND).entity("Entity not found for ID: " + apartment.id).build();
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") Long id) {
        return apartmentService.delete(id) ? Response.noContent().build()
                : Response.status(Response.Status.NOT_FOUND).entity("Entity not found for ID: " + id).build();
    }
}
