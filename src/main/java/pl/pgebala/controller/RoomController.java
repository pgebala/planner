package pl.pgebala.controller;

import pl.pgebala.model.Room;
import pl.pgebala.service.RoomService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/room")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RoomController {

    @Inject
    RoomService roomService;

    @GET
    public Response getAllRooms() {
        return Response.ok(roomService.findAll()).build();
    }

    @GET
    @Path("{apartmentId}")
    public Response getAllRooms(@PathParam("apartmentId") Long apartmentId) {
        return Response.ok(roomService.findAllByApartmentId(apartmentId)).build();
    }

    @POST
    @Path("{apartmentId}")
    public Response saveRoom(@PathParam("apartmentId") Long apartmentId, Room room) {
        return Response.ok(roomService.save(apartmentId, room)).build();
    }

    @PUT
    public Response updateRoom(Room room) {
        Room updatedRoom = roomService.update(room);
        return updatedRoom != null ? Response.ok(updatedRoom).build()
                : Response.status(Response.Status.NOT_FOUND).entity("Entity not found for ID: " + room.id).build();
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") Long id) {
        return roomService.delete(id) ? Response.noContent().build()
                : Response.status(Response.Status.NOT_FOUND).entity("Entity not found for ID: " + id).build();
    }
}
