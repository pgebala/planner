package pl.pgebala.model;

public enum Unit {

    METER("metr"),
    SQUARE_METER("metr kwadratowy"),
    PIECE("sztuka"),
    HOUR("godzina");

    private String message;

    Unit(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
