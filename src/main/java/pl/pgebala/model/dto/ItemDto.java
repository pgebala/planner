package pl.pgebala.model.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Builder;
import lombok.Data;
import pl.pgebala.model.Item;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@RegisterForReflection
public class ItemDto {

    private Long id;
    private String name;
    private String type;
    private String unit;
    private double quantity;
    private double price;
    private String description;

    public static ItemDto toDto(Item entity) {
        return ItemDto.builder()
                .id(entity.id)
                .name(entity.name)
                .type(entity.type.getMessage())
                .unit(entity.unit.getMessage())
                .quantity(entity.quantity)
                .price(entity.price)
                .description(entity.description)
                .build();
    }

    public static List<ItemDto> toDtos(List<Item> entities) {
        return entities.stream().map(ItemDto::toDto).collect(Collectors.toList());
    }
}
