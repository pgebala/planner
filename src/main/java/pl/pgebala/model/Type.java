package pl.pgebala.model;

public enum Type {

    SUPPLY("zasoby"),
    SERVICE("usługa"),
    EQUIPMENT("wyposażenie");

    private String message;

    Type(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
