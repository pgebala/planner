package pl.pgebala.model;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity(name = "apartment")
public class Apartment extends PanacheEntity {

    public String name;

    @OneToMany(mappedBy = "apartment", cascade = CascadeType.DETACH, orphanRemoval = true, fetch = FetchType.EAGER)
    @JsonbTransient
    public Set<Room> rooms;
}
