package pl.pgebala.model;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.PanacheQuery;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.Set;

@Entity(name = "room")
public class Room extends PanacheEntity {

    public String name;

    @ManyToOne
    @JoinColumn(name = "apartment_id")
    @JsonbTransient
    public Apartment apartment;

    @OneToMany(mappedBy = "room", cascade = CascadeType.DETACH, orphanRemoval = true, fetch = FetchType.EAGER)
    @JsonbTransient
    public Set<Item> items;

    public static PanacheQuery<PanacheEntityBase> findAllByApartmentId(Long id){
        return find("apartment.id", id);
    }
}
