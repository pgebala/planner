package pl.pgebala.model;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.PanacheQuery;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;

@Entity(name = "item")
public class Item extends PanacheEntity {

    @Column(unique = true)
    public String name;
    @Enumerated(EnumType.STRING)
    public Type type;
    @Enumerated(EnumType.STRING)
    public Unit unit;
    public double quantity;
    public double price;
    public String description;

    @ManyToOne
    @JoinColumn(name = "room_id")
    @JsonbTransient
    public Room room;

    public static Item findByName(String name){
        return find("name", name).firstResult();
    }

    public static PanacheQuery<PanacheEntityBase> findAllByRoomId(Long id){
        return find("room.id", id);
    }
}
