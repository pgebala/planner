package pl.pgebala.service;

import pl.pgebala.model.Room;

import java.util.List;

public interface RoomService {

    List<Room> findAll();

    List<Room> findAllByApartmentId(Long id);

    Room save(Long apartmentId, Room room);

    Room update(Room room);

    boolean delete(long id);
}
