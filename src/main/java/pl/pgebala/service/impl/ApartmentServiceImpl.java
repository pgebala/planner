package pl.pgebala.service.impl;

import pl.pgebala.model.Apartment;
import pl.pgebala.service.ApartmentService;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class ApartmentServiceImpl implements ApartmentService {

    @Override
    public List<Apartment> findAll() {
        return Apartment.findAll().list();
    }

    @Transactional
    @Override
    public Apartment save(Apartment apartment) {
        Apartment.persist(apartment);
        return apartment;
    }

    @Transactional
    @Override
    public Apartment update(Apartment apartment) {
        Apartment entity = Apartment.findById(apartment.id);
        if(entity != null) {
            entity.name = apartment.name;
            entity.persist();
        }
        return entity;
    }

    @Transactional
    @Override
    public boolean delete(long id) {
        return Apartment.deleteById(id);
    }
}
