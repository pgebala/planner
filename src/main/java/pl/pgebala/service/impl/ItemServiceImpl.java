package pl.pgebala.service.impl;

import pl.pgebala.model.Item;
import pl.pgebala.model.Room;
import pl.pgebala.model.dto.ItemDto;
import pl.pgebala.service.ItemService;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class ItemServiceImpl implements ItemService {

    @Override
    public List<ItemDto> findAll() {
        return ItemDto.toDtos(Item.findAll().list());
    }

    @Override
    public List<ItemDto> findAllByRoomId(Long id) {
        return ItemDto.toDtos(Item.findAllByRoomId(id).list());
    }

    @Override
    public ItemDto findByName(String name) {
        return ItemDto.toDto(Item.findByName(name));
    }

    @Transactional
    @Override
    public ItemDto save(Long roomId, Item item) {
        Room room = new Room();
        room.id = roomId;
        item.room = room;
        Item.persist(item);
        return ItemDto.toDto(item);
    }

    @Transactional
    @Override
    public ItemDto update(Item item) {
        Item entity = Item.findById(item.id);
        if(entity != null) {
            entity.name = item.name;
            entity.type = item.type;
            entity.unit = item.unit;
            entity.quantity = item.quantity;
            entity.price = item.price;
            entity.description = item.description;
            entity.persist();
        }
        return ItemDto.toDto(entity);
    }

    @Transactional
    @Override
    public boolean delete(Long id) {
        return Item.deleteById(id);
    }
}
