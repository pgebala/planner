package pl.pgebala.service.impl;

import pl.pgebala.model.Apartment;
import pl.pgebala.model.Room;
import pl.pgebala.service.RoomService;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class RoomServiceImpl implements RoomService {

    @Override
    public List<Room> findAll() {
        return Room.findAll().list();
    }

    @Override
    public List<Room> findAllByApartmentId(Long id) {
        return Room.findAllByApartmentId(id).list();
    }

    @Transactional
    @Override
    public Room save(Long apartmentId, Room room) {
        Apartment apartment = new Apartment();
        apartment.id = apartmentId;
        room.apartment = apartment;
        Room.persist(room);
        return room;
    }

    @Transactional
    @Override
    public Room update(Room room) {
        Room entity = Room.findById(room.id);
        if(entity != null) {
            entity.name = room.name;
            entity.persist();
        }
        return entity;
    }

    @Transactional
    @Override
    public boolean delete(long id) {
        return Room.deleteById(id);
    }
}
