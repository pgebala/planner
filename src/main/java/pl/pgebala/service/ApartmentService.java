package pl.pgebala.service;

import pl.pgebala.model.Apartment;

import java.util.List;

public interface ApartmentService {

    List<Apartment> findAll();

    Apartment save(Apartment apartment);

    Apartment update(Apartment apartment);

    boolean delete(long id);
}
