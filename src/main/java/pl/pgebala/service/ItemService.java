package pl.pgebala.service;

import pl.pgebala.model.Item;
import pl.pgebala.model.dto.ItemDto;

import java.util.List;

public interface ItemService {

    List<ItemDto> findAll();

    List<ItemDto> findAllByRoomId(Long id);

    ItemDto findByName(String name);

    ItemDto save(Long roomId, Item item);

    ItemDto update(Item item);

    boolean delete(Long id);
}
