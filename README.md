## Quarkus EDU

Celem zadania było zapoznanie się z frameworkiem Quarkus, który służy do budowania aplikacji webowych.

#### 1. Przygotowanie projektu

- JDK 11 (minimum JDK 8)
- GraalVM CE Java 11 (dla natywnej kompilacji)
- Apache Maven 3.6.3 (minimum 3.6.2 lub Gradle)
- IntelliJ IDEA (z pluginami: Quarkus Integration, Quarkus Tools)
- MySQL (https://hub.docker.com/_/mysql)
- AngularJS + Bootstrap 3

Projekt Quarkus można wygenerować na stronie https://code.quarkus.io/

Należy ustawić odpowiednie groupId, artifactId i narzędzie do budowania. Następnie z dostępnej listy wybrać zależności, które będą użyte w aplikacji (można je również dodać później).
Mój bazowy projekt zawierał:
- RESTEasy JAX-RS
- RESTEasy JSON-B
- Hibernate ORM
- JDBC Driver - MySQL
- YAML Configuration.

Po pobraniu projektu można go zaimportować do IDE i od razu rozpocząć pracę. W strukturze projektu można znaleźć:
- klasę z przykładowym kontrolerem
- stronę uruchomianą na starcie
- plik application.properties
- dwa pliki Dockerfile, dla kompilacji standardowej oraz natywnej

#### 2. Budowanie i uruchomienie aplikacji

Uruchomienie aplikacji w trybie deweloperskim:
```
mvn quarkus:dev
```
Budowanie pliku dla JVM `planner-1.0.0-SNAPSHOT-runner.jar`:
```
mvn package
```
Uruchomienie:
```
java -jar target/planner-1.0.0-SNAPSHOT-runner.jar
```

Budowanie pliku natywnego `planner-1.0.0-SNAPSHOT-runner.jar`:
```
mvn package -Pnative -Dquarkus.native.container-build=true
```
Uruchomienie:
```
./planner-1.0.0-SNAPSHOT-runner
```

#### 3. Doświadczenia z pracy z Quarkusem

- Live reload działa bardzo dobrze. Podczas pracy w trybie deweloperskim prawie w ogóle nie musimy restartować aplikacji, żeby zobaczyć zmiany w kodzie, co bardzo przyspiesza pracę.
- Aplikacja startuje dość szybko. Dla JVM było to średnio 2.359s, natomiast w trybie natywnym zaledwie w ok. 0.415s.
- Bardzo dobrze wygląda wykorzystanie pamięci działającej aplikacji. Dla JVM było to ok. 194.0 MB, natomiast w trybie natywnym tylko 87.2 MB.
- Podczas kompilacji natywnej tworzony jest graf zależności pomiędzy klasami, polami i metodami, a następnie wycinane są te niepotrzebne. Operacja ta trwa dość długo, jednak dzięki temu aplikacja wstaje dużo szybciej i zajmuje mniej pamięci. Jeżeli chcemy korzystać z refleksji, musimy oznaczyć klasy specjalną adnotacją lub umieścić je w odpowiednim pliku konfiguracyjnym.
- Przy tworzeniu projektu wybieramy tylko te zależności, które są nam potrzebne. Nie mamy tutaj "starterów" jak w SpringBoot, które dokładają nam zbędne biblioteki.
- Quarkus korzysta z nieco innych adnotacji niż SpringBoot np. `@Autowired > @Inject`, `@RequestMapping > @Path`, `@PostMapping > @POST`, `@Service,@Component > @ApplicationScoped`, znanych z Java EE.
- Praca z bazą danych jest w porządku, o ile wykorzystamy bibliotekę quarkus-hibernate-orm-panache, która pozwala stworzyć encje i repozytoria z kilkoma przydatnymi metodami np. `Item.findAll()`. Rozwiązanie to nie jest jednak tak wygodne, jak to dostarczone w `spring-data-jpa`.
- Konfiguracja aplikacji odbywa się w pliku `application.properties` (lub `.yml`). Dla wersji produkcyjnej możemy użyć profilu np. `%prod.quarkus.hibernate-orm.sql-load-script=import.sql`
- Łatwo można wykorzystać narzędzia do monitorowania, dodając odpowiednie zależności do pom'a np. `quarkus-smallrye-openapi`, `quarkus-smallrye-health`, `quarkus-smallrye-metrics`, `quarkus-smallrye-opentracing`
- Quarkus wykorzystuje do testów JUnit5 z Mockito oraz rest-assured do testowania restowego API. Klasa testowa oznaczona jest adnotacją @QuarkusTest.
- Poza klasycznymi testami można dodać też testy dla wersji natywnej aplikacji, wykorzystując adnotację `@NativeImageTest`.
- Szybki czas uruchomienia oraz przygotowane pliki Dockerfile wskazują na to, że Quarkus jest zaprojektowany tak, aby dobrze działał w rozwiązaniach chmurowych.
- Quarkus zapewnia wsparcie dla wielu rozwiązań m.in. baz SQL i noSQL, kolejek, security, chmury i integracji. Listę wszystkich rozszerzeń można znaleźć na https://code.quarkus.io/
